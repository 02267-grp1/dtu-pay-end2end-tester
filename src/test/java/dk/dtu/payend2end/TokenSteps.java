package dk.dtu.payend2end;

import dk.dtu.models.Token;
import dk.dtu.services.CustomerService;
import dk.dtu.services.PaymentService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.List;
/**
    The author of each cucumber scenario stated in the .feature file was responsible for the implementation of the used steps

    feature file: token.feature
 */
public class TokenSteps {
    CustomerService client = new CustomerService();

    String customerId;

    List<Token> tokens;
    @Given("the id of a custommer {string}")
    public void theIdOfACustommer(String arg0) {
        this.customerId = arg0;
    }

    @And("the custommer requests tokens for transactions")
    public void theCustommerRequestsTokensForTransactions() {
        this.tokens = client.getTokens(customerId);
    }

    @Then("{int} tokens are returned with the credentials of the custommer")
    public void tokensAreReturnedWithTheCredentialsOfTheCustommer(int arg0) {
        Assert.assertEquals(arg0, tokens.size());
        for(Token t : tokens) {
            Assert.assertEquals(customerId, t.getCustomerId());
        }
    }
}
