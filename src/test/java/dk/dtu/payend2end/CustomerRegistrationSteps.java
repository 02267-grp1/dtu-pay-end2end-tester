package dk.dtu.payend2end;

import dk.dtu.models.Customer;
import dk.dtu.services.CustomerService;
import fastmoney.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jakarta.ws.rs.core.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.UUID;
/**
    The author of each cucumber scenario stated in the .feature file was responsible for the implementation of the used steps

    feature file: registerCustomer.feature
 */
public class CustomerRegistrationSteps {

    public Customer customer = new Customer();

    private final BankService bankService = new BankServiceService().getBankServicePort();
    private final CustomerService client = new CustomerService();
    public static UUID customerId;
    private Response resultDeregister;

    @Given("the name of the customer {string} {string}")
    public void theNameOfTheCustomer(String arg0, String arg1) {
        customer.setFirstName(arg0);
        customer.setLastName(arg1);
    }

    @And("the cpr of the customer {string}")
    public void theCprOfTheCustomer(String arg0) {
        customer.setCpr(arg0);
    }

    @And("the customer has a bank account")
    public void theCustomerHasABankAccount() throws BankServiceException_Exception {
        User bankAccount =  new User();
        bankAccount.setFirstName(customer.getFirstName());
        bankAccount.setLastName(customer.getLastName());
        bankAccount.setCprNumber(customer.getCpr());
        var accountId = bankService.createAccountWithBalance(bankAccount, BigDecimal.valueOf(1000));
        System.out.println("New customer bank account: \n"+ accountId);
        customer.setBankAccountId(accountId);
    }

    @When("the customer registers at DTU Pay")
    public void theCustomerRegistersAtDTUPay() {
        customerId = client.registration(customer);
    }

    @Then("the customer is successfully registered")
    public void theCustomerIsSuccessfullyRegistered() {
        Assert.assertNotNull(customerId);
        System.out.println("New customer user id: \n"+ customerId);
    }

    @And("the customer's bank account is retired")
    public void theCustomerSBankAccountIsRetired() {
        try {
            bankService.retireAccount(this.customer.getBankAccountId());
        } catch (BankServiceException_Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @When("the customer is deregistered at DTU Pay")
    public void theCustomerIsDeregisteredAtDTUPay() {
        resultDeregister = client.deregistration(customerId.toString());
    }

    @Then("the customer is successfully deregistered")
    public void theCustomerIsSuccessfullyDeregistered() {
        Assert.assertEquals(Response.Status.OK.getStatusCode(), resultDeregister.getStatus());
    }
}
