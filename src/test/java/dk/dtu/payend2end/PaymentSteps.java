package dk.dtu.payend2end;

import dk.dtu.models.Customer;
import dk.dtu.models.Token;
import fastmoney.*;
import dk.dtu.models.Merchant;
import dk.dtu.models.Payment;
import dk.dtu.services.CustomerService;
import dk.dtu.services.MerchantService;
import dk.dtu.services.PaymentService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.List;
/**
    The author of each cucumber scenario stated in the .feature file was responsible for the implementation of the used steps

    feature file: payment.feature
 */
public class PaymentSteps {

    String customerAccountId, merchantAccountId;
    Token customerToken;
    int amount;

    Customer customer = new Customer();
    Merchant merchant;

    String customerRegistered, merchantRegistered;

    private final CustomerService customerService = new CustomerService();
    private final MerchantService merchantService = new MerchantService();
    private final PaymentService paymentService = new PaymentService();

    private final BankService bankService = new BankServiceService().getBankServicePort();

    @Given("the customer bank account is created with CPR {string} and name {string} {string}")
    public void theCustomerBankAccountIsCreatedWithCPRAndName(String cpr, String firstName, String lastName) throws BankServiceException_Exception {
        User userCustomer = new User();
        userCustomer.setCprNumber(cpr);
        userCustomer.setFirstName(firstName);
        userCustomer.setLastName(lastName);

        this.customerAccountId = bankService.createAccountWithBalance(userCustomer, BigDecimal.valueOf(1000));
    }

    @And("the merchant bank account is created with CPR {string} and name {string} {string}")
    public void theMerchantBankAccountIsCreatedWithCPRAndName(String cpr, String firstName, String lastName) throws BankServiceException_Exception {
        User userMerchant = new User();
        userMerchant.setCprNumber(cpr);
        userMerchant.setFirstName(firstName);
        userMerchant.setLastName(lastName);

        this.merchantAccountId = bankService.createAccountWithBalance(userMerchant, BigDecimal.valueOf(1000));
    }

    @Given("the customer is registered with CPR {string} and name {string} {string}")
    public void theCustomerIsRegisteredWithCPRAndName(String cpr, String firstName, String lastName) {
        this.customer = new Customer();
        this.customer.setCpr(cpr);
        this.customer.setBankAccountId(customerAccountId);
        this.customer.setFirstName(firstName);
        this.customer.setLastName(lastName);

        this.customerRegistered = customerService.registration(this.customer).toString();
        Assert.assertNotEquals("", customerRegistered);
    }

    @And("the merchant is registered with CPR {string} and name {string} {string}")
    public void theMerchantIsRegisteredWithCPRAndName(String cpr, String firstName, String lastName) {
        this.merchant = new Merchant();
        this.merchant.setBankAccountId(merchantAccountId);
        this.merchant.setFirstName(firstName);
        this.merchant.setLastName(lastName);
        this.merchant.setCpr(cpr);

        this.merchantRegistered = merchantService.registration(this.merchant).toString();
        Assert.assertNotEquals("", merchantRegistered);
    }

    @And("the customer token is valid")
    public void theCustomerTokenIsValid() {
        List<Token> tokens = customerService.getTokens(this.customerRegistered);
        this.customerToken = tokens.getFirst();
        Assert.assertNotEquals("", this.customerToken.getUuid());
    }

    @When("a merchant requests a payment for {int} kr")
    public void aMerchantRequestsPaymentForKr(Integer amount) {
        this.amount = amount;
    }

    @Then("initiate money transfer")
    public void initiateMoneyTransfer() {
        Payment payload = new Payment(amount, customerToken.getUuid(), merchantRegistered);
        Assert.assertEquals("Payment successful", paymentService.payment(payload));
    }

    @And("retire bank accounts")
    public void retireBankAccounts() throws BankServiceException_Exception {
        bankService.retireAccount(this.customer.getBankAccountId());
        bankService.retireAccount(this.merchant.getBankAccountId());
    }

}
