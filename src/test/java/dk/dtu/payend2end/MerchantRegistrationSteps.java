package dk.dtu.payend2end;

import dk.dtu.models.Merchant;
import dk.dtu.services.MerchantService;
import fastmoney.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jakarta.ws.rs.core.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.UUID;
/**
    The author of each cucumber scenario stated in the .feature file was responsible for the implementation of the used steps

    feature file: registerMerchant.feature
 */
public class MerchantRegistrationSteps {

    public Merchant merchant = new Merchant();

    private final BankService bankService = new BankServiceService().getBankServicePort();
    private final MerchantService client = new MerchantService();
    public static UUID merchantId;
    private Response resultDeregister;

    @Given("the name of the merchant {string} {string}")
    public void theNameOfTheMerchant(String arg0, String arg1) {
        merchant.setFirstName(arg0);
        merchant.setLastName(arg1);
    }

    @And("the cpr of the merchant {string}")
    public void theCprOfTheMerchant(String arg0) {
        merchant.setCpr(arg0);
    }

    @And("the merchant has a bank account")
    public void theMerchantHasABankAccount() throws BankServiceException_Exception {
        User bankAccount =  new User();
        bankAccount.setFirstName(merchant.getFirstName());
        bankAccount.setLastName(merchant.getLastName());
        bankAccount.setCprNumber(merchant.getCpr());
        var accountId = bankService.createAccountWithBalance(bankAccount, BigDecimal.valueOf(1000));
        System.out.println("New Merchant bank account: \n"+ accountId);
        merchant.setBankAccountId(accountId);
    }

    @When("the merchant registers at DTU Pay")
    public void theMerchantRegistersAtDTUPay() {
        merchantId = client.registration(merchant);
    }

    @Then("the merchant is successfully registered")
    public void theMerchantIsSuccessfullyRegistered() {
        Assert.assertNotNull(merchantId);
        System.out.println("New Merchant user id: \n"+ merchantId);
    }

    @And("the merchant's bank account is retired")
    public void theMerchantSBankAccountIsRetired() {
        try {
            bankService.retireAccount(this.merchant.getBankAccountId());
        } catch (BankServiceException_Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @When("the merchant is deregistered at DTU Pay")
    public void theMerchantIsDeregisteredAtDTUPay() {
        resultDeregister = client.deregistration(merchantId.toString());
    }

    @Then("the merchant is successfully deregistered")
    public void theMerchantIsSuccessfullyDeregistered() {
        Assert.assertEquals(Response.Status.OK.getStatusCode(), resultDeregister.getStatus());
    }

//    @When("the Merchant is deregistered at DTU Pay")
//    public void theMerchantIsDeregisteredAtDTUPay() {
//        resultDeregister = client.deregistration(MerchantId.toString());
//    }
//
//    @Then("the Merchant is successfully deregistered")
//    public void theMerchantIsSuccessfullyDeregistered() {
//        Assert.assertEquals(Response.Status.OK.getStatusCode(), resultDeregister.getStatus());
//    }
}
