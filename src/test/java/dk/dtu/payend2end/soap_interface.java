package dk.dtu.payend2end;

import fastmoney.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
/**
    The author of each cucumber scenario stated in the .feature file was responsible for the implementation of the used steps

    feature file: soap_interface.feature
 */
public class soap_interface {
    BankService bank = new BankServiceService().getBankServicePort();

    User temp_customer = new User();
    User temp_merchant = new User();

    public static String temp_customer_accountID;
    public static String temp_merchant_accountID;

    BigDecimal good_amount_to_pay = new BigDecimal(100);
    BigDecimal bad_amount_to_pay = new BigDecimal(2000);

    String transfer_response;


    @Given("a customer with cpr {string}, first name {string}, last name {string}, and balance {string}")
    public void aCustomerWithCprFirstNameLastNameAndBalance(String customer_cpr, String customer_first_name,
                                                            String customer_last_name, String customer_new_balance) {
        temp_customer.setCprNumber(customer_cpr);
        temp_customer.setFirstName(customer_first_name);
        temp_customer.setLastName(customer_last_name);
        BigDecimal initial_account_balance = new BigDecimal(customer_new_balance);
        try {
            temp_customer_accountID = bank.createAccountWithBalance(temp_customer, initial_account_balance);
        } catch (BankServiceException_Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @When("customer creates a bank account")
    public void customerCreatesBankAccount() {
        try {
            assertNotNull(bank.getAccount(temp_customer_accountID));
        } catch (BankServiceException_Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @Then("a bank account for customer is registered successfully")
    public void aBankAccountForCustomerIsCreatedSuccessfullyWithBalance() {
        try {
            assertNotNull(bank.getAccount(temp_customer_accountID));
        } catch (BankServiceException_Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Given("a merchant with cpr {string}, first name {string}, last name {string}, and balance {string}")
    public void aMerchantWithCprFirstNameLastNameAndBalance(String merchant_cpr, String merchant_first_name, String merchant_last_name, String new_merchant_balance) {
        temp_merchant.setCprNumber(merchant_cpr);
        temp_merchant.setFirstName(merchant_first_name);
        temp_merchant.setLastName(merchant_last_name);
        BigDecimal initial_account_balance = new BigDecimal(new_merchant_balance);

        try {
            temp_merchant_accountID = bank.createAccountWithBalance(temp_merchant, initial_account_balance);
        } catch (BankServiceException_Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @When("merchant creates a bank account")
    public void merchantCreatesABankAccount() {
        try {
            assertNotNull(bank.getAccount(temp_merchant_accountID));
        } catch (BankServiceException_Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Then("a bank account for merchant is registered successfully")
    public void aBankAccountForMerchantIsCreatedSuccessfully() {
        try {
            assertNotNull(bank.getAccount(temp_merchant_accountID));
        } catch (BankServiceException_Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Given("a known customer bank account")
    public void aKnownCustomerBankAccount() {
        try {
            assertNotNull(bank.getAccount(temp_customer_accountID));
        } catch (BankServiceException_Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @And("a known merchant bank account")
    public void aKnownMerchantBankAccount() {
        try {
            assertNotNull(bank.getAccount(temp_merchant_accountID));
        } catch (BankServiceException_Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @When("a customer transferres a good amount of money \\({string}) to merchant")
    public void aCustomerTransferresAGoodAmountOfMoneyToMerchant(String money_to_transfer) {

        boolean money_to_transfer_is_good;
        money_to_transfer_is_good = new BigDecimal(money_to_transfer).equals(good_amount_to_pay);

        assertTrue(money_to_transfer_is_good);

    }

    @When("a customer transferres a bad amount of money \\({string}) to merchant")
    public void aCustomerTransferresABadAmountOfMoneyToMerchant(String money_to_transfer) {
        boolean money_to_transfer_is_bad;
        money_to_transfer_is_bad = new BigDecimal(money_to_transfer).equals(bad_amount_to_pay);

        assertTrue(money_to_transfer_is_bad);
    }

    @Then("money is transferred successfully")
    public void moneyIsTransferredSuccessfully() throws BankServiceException_Exception {
        try {
            bank.transferMoneyFromTo(temp_customer_accountID, temp_merchant_accountID, good_amount_to_pay, "good");
        } catch (BankServiceException_Exception e) {
            transfer_response = e.getMessage();
            System.out.println(transfer_response);
        }

    }

    @Then("money is not transferred")
    public void moneyIsNotTransferred() throws BankServiceException_Exception {
        try {
            bank.transferMoneyFromTo(temp_customer_accountID, temp_merchant_accountID, bad_amount_to_pay, "bad");
        } catch (BankServiceException_Exception e) {
            transfer_response = e.getMessage();
            System.out.println(transfer_response);
        }


        bank.retireAccount(temp_customer_accountID);
        bank.retireAccount(temp_merchant_accountID);
    }


}
