package dk.dtu.models;

import lombok.Data;
/**
    @author s194626
 */
@Data
public class Customer {
    private String firstName, lastName, cpr, bankAccountId;

}
