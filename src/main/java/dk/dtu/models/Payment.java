package dk.dtu.models;

import lombok.AllArgsConstructor;
import lombok.Data;
/**
    @author s222994
 */
@Data
@AllArgsConstructor
public class Payment {
    private Integer amount;
    private String customerToken;
    private String merchantId;
}
