package dk.dtu.models;

import lombok.Data;
/**
    @author s213555
 */
@Data
public class Merchant {
    private String firstName;
    private String lastName;
    private String cpr;
    private String bankAccountId;
}
