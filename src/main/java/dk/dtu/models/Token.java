package dk.dtu.models;

import lombok.Data;

import java.io.Serializable;
/**
    @author s153277
 */
@Data
public class Token implements Serializable {
    private String customerId;
    private String uuid;
}
