package dk.dtu.services;

import dk.dtu.models.Payment;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;

public class PaymentService {

    private final Client client = ClientBuilder.newClient();
    private final WebTarget r = client.target("http://localhost:8080/");
    /**
        @author s222994
    */
    public String payment(Payment payload) {
        return r.path("payment")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.json(payload), String.class);
    }

}
