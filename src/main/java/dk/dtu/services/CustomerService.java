package dk.dtu.services;

import dk.dtu.models.Customer;
import dk.dtu.models.Token;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;
import java.util.UUID;

public class CustomerService {

    private final Client client = ClientBuilder.newClient();
    private final WebTarget r = client.target("http://localhost:8080/");
    /**
        @author s194626
    */
    public UUID registration(Customer customer) {
        return UUID.fromString(r.path("customer")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.json(customer), String.class));
    }
    /**
        @author s202082
    */
    public Response deregistration(String customerId) {
        return r.path("customer/" + customerId)
                        .request()
                        .delete();
    }
    /**
        @author s153277
    */
    public List<Token> getTokens(String customerId) {
        return r.path("tokens")
                .queryParam("customerId", customerId)
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<>() {
                });
    }

}
