package dk.dtu.services;

import dk.dtu.models.Merchant;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.UUID;

public class MerchantService {

    private final Client client = ClientBuilder.newClient();
    private final WebTarget r = client.target("http://localhost:8080/");
    /**
        @author s213555
    */
    public UUID registration(Merchant merchant) {
        return UUID.fromString(r.path("merchant")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.json(merchant), String.class));
    }
    /**
        @author s223142
    */
    public Response deregistration(String merchantId) {
        return r.path("merchant/" + merchantId)
                .request()
                .delete();
    }

}
