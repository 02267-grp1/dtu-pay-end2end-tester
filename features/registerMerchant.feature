Feature: register a merchant
  # s213555
  Scenario: Successfully register a merchant
    Given the name of the merchant "Shop" "Shoppington"
    And the cpr of the merchant "123456-85401"
    And the merchant has a bank account
    When the merchant registers at DTU Pay
    Then the merchant is successfully registered
    #This is only done for testing purposes to remove the registered user from the bank
    And the merchant's bank account is retired
  # s223142
  Scenario: Successfully deregister a merchant
    Given the name of the merchant "Shop" "Shoppington"
    And the cpr of the merchant "123456-85401"
    And the merchant has a bank account
    When the merchant registers at DTU Pay
    When the merchant is deregistered at DTU Pay
    Then the merchant is successfully deregistered
    #This is only done for testing purposes to remove the registered user from the bank
    And the merchant's bank account is retired


