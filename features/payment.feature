Feature: Payment
  # s222994
  Scenario: Successful Payment
    Given the customer bank account is created with CPR "123456-8540" and name "John" "Doe"
    And the merchant bank account is created with CPR "123456-8541" and name "John" "Doe"
    Given the customer is registered with CPR "123456-8540" and name "John" "Doe"
    And the merchant is registered with CPR "123456-8541" and name "Shop" "Shop"

    And the customer token is valid
    When a merchant requests a payment for 10 kr
    Then initiate money transfer
    And retire bank accounts
