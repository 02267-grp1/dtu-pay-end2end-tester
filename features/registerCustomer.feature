Feature: register a customer
  # s194626
  Scenario: Successfully register a customer
    Given the name of the customer "Hans" "Jensen"
    And the cpr of the customer "123456-8542"
    And the customer has a bank account
    When the customer registers at DTU Pay
    Then the customer is successfully registered
    #This is only done for testing purposes to remove the registered user from the bank
    And the customer's bank account is retired
  # s202082
  Scenario: Successfully deregister a customer
    Given the name of the customer "Jens" "Hansen"
    And the cpr of the customer "123456-8543"
    And the customer has a bank account
    And the customer registers at DTU Pay
    When the customer is deregistered at DTU Pay
    Then the customer is successfully deregistered
    And the customer's bank account is retired