Feature: Bank account operations
  # s202082
  Scenario: A bank account has been created successfully for a customer
    Given a customer with cpr "123456-8544", first name "long_customer", last name "johnson", and balance "1000"
    When customer creates a bank account
    Then a bank account for customer is registered successfully
  # s202082
  Scenario: A bank account has been created successfully for a merchant
    Given a merchant with cpr "123456-8545", first name "max_merchant", last name "power", and balance "1000"
    When merchant creates a bank account
    Then a bank account for merchant is registered successfully
  # s202082
  Scenario: A customer bank account had enough money to transfer to merchant
      Given a known customer bank account
      And a known merchant bank account
      When a customer transferres a good amount of money ("100") to merchant
      Then money is transferred successfully
  # s223142
  Scenario: A customer bank account  did not have enough money to transfer to merchant
    Given a known customer bank account
    And a known merchant bank account
    When a customer transferres a bad amount of money ("2000") to merchant
    Then money is not transferred
